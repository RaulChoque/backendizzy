package com.buildssi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildssiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildssiApplication.class, args);
    }
}
