package com.buildssi.services;

import com.buildssi.model.Cargo;

import java.util.List;

public interface CargoService extends GenericService<Cargo> {
    
    List<Cargo> findAll();

    List<Cargo> find(Long id);
}
