package com.buildssi.services;

import com.buildssi.model.Empleado;
import com.buildssi.repositories.EmpleadoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
public class EmpleadoServiceImpl extends GenericServiceImpl<Empleado> implements EmpleadoService {
    private static Logger logger = LoggerFactory.getLogger(EmpleadoServiceImpl.class);
    private EmpleadoRepository empleadoRepository;

    public EmpleadoServiceImpl(EmpleadoRepository empleadoRepository) {
        this.empleadoRepository = empleadoRepository;
    }

    @Override
    protected CrudRepository<Empleado, Long> getRepository() {
        return empleadoRepository;
    }

    @Override
    public void saveImage(Long id, InputStream file) {
        Empleado empleadoPersisted = findById(id);
        try {
            Byte[] bytes = ImageUtils.inputStreamToByteArray(file);
            empleadoPersisted.setFoto(bytes);
            getRepository().save(empleadoPersisted);
        } catch (IOException e) {
            logger.error("Error reading file", e);
            e.printStackTrace();
        }
    }
}
