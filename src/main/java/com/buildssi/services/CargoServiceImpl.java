package com.buildssi.services;

import com.buildssi.model.Cargo;
import com.buildssi.repositories.CargoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class CargoServiceImpl extends GenericServiceImpl<Cargo> implements CargoService {
    private CargoRepository cargoRepository;

    public CargoServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    protected CrudRepository<Cargo, Long> getRepository() {
        return cargoRepository;
    }

    @Override
    public List<Cargo> findAll() {
        return super.findAll();
    }

    @Override
    public List<Cargo> find(Long idObra) {
        return StringUtils.isEmpty(idObra) ? findAll() : (List<Cargo>) cargoRepository.findById(idObra).get();
    }
}
