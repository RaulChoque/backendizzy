package com.buildssi.services;

import com.buildssi.model.Empleado;

import java.io.InputStream;
import java.util.List;

public interface EmpleadoService extends GenericService<Empleado> {

    List<Empleado> findAll();

    void saveImage(Long id, InputStream inputStream);
}
