/**
 * @autor: Raúl Choque
 **/
package com.buildssi.services;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public abstract class GenericServiceImpl<T> implements GenericService<T> {

    protected abstract CrudRepository<T, Long> getRepository();

    public List<T> findAll() {
        List<T> listT = new ArrayList<>();
        getRepository().findAll().forEach(listT::add);
        return listT;
    }

    @Override
    public T findById(Long id) {
        return getRepository().findById(id).get();
    }

    @Override
    public T save(T model) {
        return getRepository().save(model);
    }

    @Override
    public void deleteById(Long id) {
        getRepository().deleteById(id);
    }
}
