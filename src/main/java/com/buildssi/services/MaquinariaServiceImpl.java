/**
 * @autor: Raúl Choque
 **/
package com.buildssi.services;

import com.buildssi.command.MaquinariaCommand;
import com.buildssi.model.Estado;
import com.buildssi.model.Maquinaria;
import com.buildssi.model.Proveedor;
import com.buildssi.repositories.EstadoRepository;
import com.buildssi.repositories.MaquinariaRepository;
import com.buildssi.repositories.ProveedorRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class MaquinariaServiceImpl extends GenericServiceImpl<Maquinaria> implements MaquinariaService {

    private MaquinariaRepository maquinariaRepository;
    private ProveedorRepository proveedorRepository;
    private EstadoRepository estadoRepository;

    public MaquinariaServiceImpl(MaquinariaRepository maquinariaRepository, ProveedorRepository proveedorRepository, EstadoRepository estadoRepository) {
        this.maquinariaRepository = maquinariaRepository;
        this.proveedorRepository = proveedorRepository;
        this.estadoRepository = estadoRepository;
    }

    @Override
    protected CrudRepository<Maquinaria, Long> getRepository() {
        return maquinariaRepository;
    }

    @Override
    public List<MaquinariaCommand> getMaquinarias() {

        List<MaquinariaCommand> list = new ArrayList<>();
        maquinariaRepository.findAll().forEach(maquinaria -> {
            if(!maquinaria.getEstadoDelete()) {
                MaquinariaCommand maquinariaCommand = new MaquinariaCommand(maquinaria);
                list.add(maquinariaCommand);
            }
        });
        return list;
    }

    @Override
    public MaquinariaCommand getMaquinariaByID(Long id) {

        Maquinaria maquinaria = maquinariaRepository.findById(id).get();
        return new MaquinariaCommand(maquinaria);
    }

    @Override
    public MaquinariaCommand createMaquinaria(MaquinariaCommand maquinariaCommand) {

        Proveedor proveedor = proveedorRepository.findById(maquinariaCommand.getIdProveedor()).get();
        Estado estado = estadoRepository.findById(maquinariaCommand.getIdEstado()).get();
        Maquinaria maquinaria = maquinariaCommand.toModel();
        maquinaria.setProveedor(proveedor);
        maquinaria.setEstado(estado);
        maquinariaRepository.save(maquinaria);
        return new MaquinariaCommand(maquinaria);
    }

    @Override
    public MaquinariaCommand updateMaquinaria(MaquinariaCommand maquinariaCommand) {

        Proveedor proveedor = proveedorRepository.findById(maquinariaCommand.getIdProveedor()).get();
        Estado estado = estadoRepository.findById(maquinariaCommand.getIdEstado()).get();
        Maquinaria maquinaria = maquinariaRepository.findById(maquinariaCommand.getId()).get();
        maquinaria.setDescripcion(maquinariaCommand.getDescripcion());
        maquinaria.setFechaActivacion(maquinariaCommand.getFechaActivacion());
        maquinaria.setFechaBaja(maquinariaCommand.getFechaBaja());
        maquinaria.setValorAdquisicion(maquinariaCommand.getValorAdquisicion());
        maquinaria.setProveedor(proveedor);
        maquinaria.setEstado(estado);
        maquinariaRepository.save(maquinaria);
        return new MaquinariaCommand(maquinaria);
    }

    @Override
    public void deleteMaquinariaById(Long id) {

        Maquinaria maquinaria = maquinariaRepository.findById(id).get();
        maquinaria.setEstadoDelete(true);
        maquinariaRepository.save(maquinaria);
    }
}
