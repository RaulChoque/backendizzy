/**
 * @author: raulchoque
 **/
package com.buildssi.services;

import com.buildssi.command.EstadoCommand;
import com.buildssi.model.Estado;

import java.util.List;

public interface EstadoService extends GenericService<Estado> {
    List<EstadoCommand> getEstados();

    EstadoCommand getEstadoByID(Long id);


    EstadoCommand createEstado(EstadoCommand estadoCommand);

    EstadoCommand updateEstado(EstadoCommand estadoCommand);

    void deleteEstById(Long id);

}
