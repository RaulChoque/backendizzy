/**
 * @author: raulchoque
 **/
package com.buildssi.services;

import com.buildssi.command.TipoDocumentoCommand;
import com.buildssi.model.TipoDocumento;

import java.util.List;

public interface TipoDocumentoService extends GenericService<TipoDocumento> {
    List<TipoDocumentoCommand> getTipoDoc();

    TipoDocumentoCommand getTipoDocById(Long id);

    TipoDocumentoCommand createTipoDoc(TipoDocumentoCommand tipoDocumentoCommand);

    TipoDocumentoCommand updateTipoDoc(TipoDocumentoCommand tipoDocumentoCommand);

    void deleteTipoDocBYID(Long id);
}
