/**
 * @author: raulchoque
 **/
package com.buildssi.services;

import com.buildssi.command.MaquinariaCommand;
import com.buildssi.model.Maquinaria;

import java.util.List;

public interface MaquinariaService extends GenericService<Maquinaria> {
    List<MaquinariaCommand> getMaquinarias();

    MaquinariaCommand getMaquinariaByID(Long id);

    MaquinariaCommand createMaquinaria(MaquinariaCommand maquinariaCommand);

    MaquinariaCommand updateMaquinaria(MaquinariaCommand maquinariaCommand);

    void deleteMaquinariaById(Long id);

}
