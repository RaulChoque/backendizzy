package com.buildssi.services;

import com.buildssi.model.EmpleadoCargoObra;

import java.util.List;

public interface EmpleadoCargoObraService extends GenericService<EmpleadoCargoObra> {
    List<EmpleadoCargoObra> findAll();

    List<EmpleadoCargoObra> find(Long id);
}
