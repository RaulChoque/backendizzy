/**
 * @author: raulchoque
 **/
package com.buildssi.services;

import com.buildssi.command.ProveedorCommand;
import com.buildssi.model.Proveedor;

import java.util.List;

public interface ProveedorService extends GenericService<Proveedor> {
    List<ProveedorCommand> getProveedores();

    ProveedorCommand getProveedorByID(Long id);

    ProveedorCommand createProveedor(ProveedorCommand proveedorCommand);

    ProveedorCommand updateProveedor(ProveedorCommand proveedorCommand);

    void deleteProveedor(Long id);

}
