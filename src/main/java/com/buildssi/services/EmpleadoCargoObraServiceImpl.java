package com.buildssi.services;

import com.buildssi.model.EmpleadoCargoObra;
import com.buildssi.repositories.EmpleadoCargoObraRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.util.StringUtils;

import java.util.List;

public class EmpleadoCargoObraServiceImpl extends GenericServiceImpl<EmpleadoCargoObra> implements EmpleadoCargoObraService {
    private EmpleadoCargoObraRepository empleadoCargoObraRepository;

    public EmpleadoCargoObraServiceImpl(EmpleadoCargoObraRepository empleadoCargoObraRepository) {
        this.empleadoCargoObraRepository = empleadoCargoObraRepository;
    }

    protected CrudRepository<EmpleadoCargoObra, Long> getRepository() {
        return empleadoCargoObraRepository;
    }

    public List<EmpleadoCargoObra> findAll() {
        return super.findAll();
    }

    @Override
    public List<EmpleadoCargoObra> find(Long idObra) {
        return StringUtils.isEmpty(idObra) ? findAll() : (List<EmpleadoCargoObra>) empleadoCargoObraRepository.findById(idObra).get();
    }
}
