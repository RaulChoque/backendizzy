package com.buildssi.services;

import com.buildssi.command.AvanceCommand;
import com.buildssi.model.Avance;

import java.util.List;

public interface AvanceService extends GenericService<Avance> {
   List<AvanceCommand> getAvances();
   List<AvanceCommand> getAvancesToObra(Long idObra);

   AvanceCommand getAvanceByID(Long id);

   AvanceCommand createAvance(AvanceCommand avanceCommand);

   AvanceCommand updateAvance(AvanceCommand avanceCommand);

   void deleteAvance(Long id);
}
