package com.buildssi.services;

import com.buildssi.command.ObraCommand;
import com.buildssi.model.Estado;
import com.buildssi.model.Obra;
import com.buildssi.repositories.EstadoRepository;
import com.buildssi.repositories.ObraRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class ObraServiceImpl extends GenericServiceImpl<Obra> implements ObraService {
    private ObraRepository obraRepository;
    private EstadoRepository estadoRepository;

    public ObraServiceImpl(ObraRepository obraRepository, EstadoRepository estadoRepository) {

        this.obraRepository = obraRepository;
        this.estadoRepository = estadoRepository;
    }
    @Override
    protected CrudRepository<Obra, Long> getRepository() {
        return obraRepository;
    }

   @Override
    public List<ObraCommand> getObras() {
        List<ObraCommand> list = new ArrayList<>();
        obraRepository.findAll().forEach(obra -> {
            if (!obra.getEstadoDelete()) {
                ObraCommand obraCommand = new ObraCommand(obra);
                list.add(obraCommand);
            }
        });
        return list;
    }

    @Override
    public ObraCommand getObraByID(Long id) {

        Obra obra = obraRepository.findById(id).get();
        return new ObraCommand(obra);
    }

    @Override
    public ObraCommand createObra(ObraCommand obraCommand) {
        Estado estado = estadoRepository.findById(obraCommand.getIdEstadoObra()).get();
        obraCommand.setEstadoObra(estado);
        Obra newObra = obraCommand.toModel();
        obraRepository.save(newObra);
        return new ObraCommand(newObra);
    }

    @Override
    public ObraCommand updateObra(ObraCommand obraCommand) {

        Obra obra = obraRepository.findById(obraCommand.getId()).get();
        obra.setNombre(obraCommand.getNombre());
        obra.setDireccion(obraCommand.getDireccion());
        obra.setCiudad(obraCommand.getCiudad());
        obra.setPresupuesto(obraCommand.getPresupuesto());
        Estado estado = estadoRepository.findById(obraCommand.getIdEstadoObra()).get();
        obra.setEstado(estado);
        obraRepository.save(obra);
        return new ObraCommand(obra);
    }

    @Override
    public void deleteObra(Long id) {

        Obra obra = obraRepository.findById(id).get();
        obra.setEstadoDelete(true);
        obraRepository.save(obra);
    }
}
