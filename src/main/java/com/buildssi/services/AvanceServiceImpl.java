package com.buildssi.services;

import com.buildssi.command.AvanceCommand;
import com.buildssi.model.Avance;
import com.buildssi.model.Obra;
import com.buildssi.repositories.AvanceRepository;
import com.buildssi.repositories.ObraRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AvanceServiceImpl extends GenericServiceImpl<Avance> implements AvanceService {
    private AvanceRepository avanceRepository;
    private ObraRepository obraRepository;

    public AvanceServiceImpl(AvanceRepository avanceRepository, ObraRepository obraRepository) {
        this.avanceRepository = avanceRepository;
        this.obraRepository=obraRepository;
    }

    @Override
    protected CrudRepository<Avance, Long> getRepository() {
        return avanceRepository;
    }

    @Override
    public List<AvanceCommand> getAvances() {
        List<AvanceCommand> list = new ArrayList<>();
        avanceRepository.findAll().forEach(avance -> {
            if (!avance.getEstadoDelete()) {
                AvanceCommand avanceCommand = new AvanceCommand(avance);
                list.add(avanceCommand);
            }
        });
        return list;
    }

    @Override
    public List<AvanceCommand> getAvancesToObra(Long idObra) {
        List<AvanceCommand> list = new ArrayList<>();
        avanceRepository.findAll().forEach(avance -> {
            if (!avance.getEstadoDelete()) {
                if(avance.getObra().getId() == idObra) {
                    AvanceCommand avanceCommand = new AvanceCommand(avance);
                    list.add(avanceCommand);
                }
            }
        });
        return list;

    }

    @Override
    public AvanceCommand getAvanceByID(Long id) {
        Avance avance = avanceRepository.findById(id).get();
        return new AvanceCommand(avance);
    }

    @Override
    public AvanceCommand createAvance(AvanceCommand avanceCommand) {

        Obra obra = obraRepository.findById(avanceCommand.getIdObra()).get();
        avanceCommand.setObra(obra);
        Avance newAvance = avanceCommand.toModel();
        avanceRepository.save(newAvance);
        return new AvanceCommand(newAvance);
    }

    @Override
    public AvanceCommand updateAvance(AvanceCommand avanceCommand) {

        Avance avance = avanceRepository.findById(avanceCommand.getId()).get();
        avance.setFecha(avanceCommand.getFecha());
        avance.setDescripcion(avanceCommand.getDescripcion());
        avance.setCosto(avanceCommand.getCosto());
        Obra obra = obraRepository.findById(avanceCommand.getIdObra()).get();
        avance.setObra(obra);
        avanceRepository.save(avance);
        return new AvanceCommand(avance);
    }

    @Override
    public void deleteAvance(Long id) {

        Avance avance = avanceRepository.findById(id).get();
        avance.setEstadoDelete(true);
        avanceRepository.save(avance);
    }
}
