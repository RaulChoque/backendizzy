/**
 * @autor: Raúl Choque
 **/
package com.buildssi.services;

import com.buildssi.command.ProveedorCommand;
import com.buildssi.model.Proveedor;
import com.buildssi.model.TipoDocumento;
import com.buildssi.repositories.ProveedorRepository;
import com.buildssi.repositories.TipoDocumentoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class ProveedorServiceImpl extends GenericServiceImpl<Proveedor> implements ProveedorService {

    private ProveedorRepository proveedorRepository;
    private TipoDocumentoRepository tipoDocumentoRepository;

    public ProveedorServiceImpl(ProveedorRepository proveedorRepository, TipoDocumentoRepository tipoDocumentoRepository) {
        this.proveedorRepository = proveedorRepository;
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    @Override
    protected CrudRepository<Proveedor, Long> getRepository() {
        return proveedorRepository;
    }

    @Override
    public List<ProveedorCommand> getProveedores() {

        List<ProveedorCommand> list = new ArrayList<>();
        proveedorRepository.findAll().forEach(proveedor -> {
            if (!proveedor.getEstadoDelete()) {
                ProveedorCommand proveedorCommand = new ProveedorCommand(proveedor);
                list.add(proveedorCommand);
            }

        });
        return list;
    }

    @Override
    public ProveedorCommand getProveedorByID(Long id) {

        Proveedor proveedor = proveedorRepository.findById(id).get();
        return new ProveedorCommand(proveedor);
    }

    @Override
    public ProveedorCommand createProveedor(ProveedorCommand proveedorCommand) {

        TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(proveedorCommand.getIdTipoDocumento()).get();
        proveedorCommand.setTipoDocumento(tipoDocumento);
        Proveedor newProveedor = proveedorCommand.toModel();
        proveedorRepository.save(newProveedor);
        return new ProveedorCommand(newProveedor);
    }

    @Override
    public ProveedorCommand updateProveedor(ProveedorCommand proveedorCommand) {

        Proveedor proveedor = proveedorRepository.findById(proveedorCommand.getId()).get();
        proveedor.setNombreComercial(proveedorCommand.getNombreComercial());
        proveedor.setRazonSocial(proveedorCommand.getRazonSocial());
        proveedor.setNumeroDocumento(proveedorCommand.getNumeroDocumento());
        proveedor.setDireccion(proveedorCommand.getDireccion());
        proveedor.setDiasPlazo(proveedorCommand.getDiasPlazo());
        proveedor.setTelefono(proveedorCommand.getTelefono());
        proveedor.setCorreo(proveedorCommand.getCorreo());
        TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(proveedorCommand.getIdTipoDocumento()).get();
        proveedor.setTipoDocumento(tipoDocumento);
        proveedorRepository.save(proveedor);
        return new ProveedorCommand(proveedor);
    }

    @Override
    public void deleteProveedor(Long id) {

        Proveedor proveedor = proveedorRepository.findById(id).get();
        proveedor.setEstadoDelete(true);
        proveedorRepository.save(proveedor);
    }

}
