/**
 * @autor: Raúl Choque
 **/
package com.buildssi.services;

import com.buildssi.command.EstadoCommand;
import com.buildssi.model.Estado;
import com.buildssi.repositories.EstadoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class EstadoServiceImpl extends GenericServiceImpl<Estado> implements EstadoService {

    private EstadoRepository  estadoRepository;

    public EstadoServiceImpl(EstadoRepository estadoRepository) {
        this.estadoRepository = estadoRepository;
    }

    @Override
    protected CrudRepository<Estado, Long> getRepository() {
        return estadoRepository;
    }

    @Override
    public List<EstadoCommand> getEstados() {

        List<EstadoCommand> listEstados = new ArrayList<>();
        estadoRepository.findAll().forEach(estado -> {
            if (!estado.getEstadoDelete()) {
                EstadoCommand estadoCommand = new EstadoCommand(estado);
                listEstados.add(estadoCommand);
            }
        });
        return listEstados;
    }

    @Override
    public EstadoCommand getEstadoByID(Long id) {

        Estado estado = estadoRepository.findById(id).get();
        return new EstadoCommand(estado);
    }

    @Override
    public EstadoCommand createEstado(EstadoCommand estadoCommand) {

        Estado estado = estadoCommand.toModel();
        estadoRepository.save(estado);
        return new EstadoCommand(estado);
    }

    @Override
    public EstadoCommand updateEstado(EstadoCommand estadoCommand) {

        Estado estado = estadoRepository.findById(estadoCommand.getId()).get();
        estado.setDescripcion(estadoCommand.getDescripcion());
        estado.setNombre(estadoCommand.getNombre());
        estadoRepository.save(estado);
        return new EstadoCommand(estado);
    }

    @Override
    public void deleteEstById(Long id) {

        Estado estado = estadoRepository.findById(id).get();
        estado.setEstadoDelete(true);
        estadoRepository.save(estado);

    }

}
