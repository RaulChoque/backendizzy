package com.buildssi.services;

import com.buildssi.command.ObraCommand;
import com.buildssi.model.Obra;

import java.util.List;

public interface ObraService extends GenericService<Obra> {
    List<ObraCommand> getObras();

    ObraCommand getObraByID(Long id);

    ObraCommand createObra(ObraCommand obraCommand);

    ObraCommand updateObra(ObraCommand obraCommand);

    void deleteObra(Long id);
}
