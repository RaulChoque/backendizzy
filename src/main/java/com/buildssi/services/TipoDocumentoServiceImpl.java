/**
 * @autor: Raúl Choque
 **/
package com.buildssi.services;

import com.buildssi.command.TipoDocumentoCommand;
import com.buildssi.model.TipoDocumento;
import com.buildssi.repositories.TipoDocumentoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class TipoDocumentoServiceImpl extends GenericServiceImpl<TipoDocumento> implements TipoDocumentoService {

    private TipoDocumentoRepository tipoDocumentoRepository;

    public TipoDocumentoServiceImpl(TipoDocumentoRepository tipoDocumentoRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
    }

    @Override
    protected CrudRepository<TipoDocumento, Long> getRepository() {
        return tipoDocumentoRepository;
    }

    @Override
    public List<TipoDocumentoCommand> getTipoDoc() {

        List<TipoDocumentoCommand> listTipoDoc = new ArrayList<>();
        tipoDocumentoRepository.findAll().forEach(tipoDocumento -> {
            if (!tipoDocumento.getEstadoDelete()) {
                TipoDocumentoCommand tipoDocumentoCommand = new TipoDocumentoCommand(tipoDocumento);
                listTipoDoc.add(tipoDocumentoCommand);
            }
        });
        return listTipoDoc;

    }

    @Override
    public TipoDocumentoCommand getTipoDocById(Long id) {

        TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(id).get();
        return new TipoDocumentoCommand(tipoDocumento);
    }

    @Override
    public TipoDocumentoCommand createTipoDoc(TipoDocumentoCommand tipoDocumentoCommand) {

        TipoDocumento tipoDocumento = tipoDocumentoCommand.toModel();
        tipoDocumentoRepository.save(tipoDocumento);
        return new TipoDocumentoCommand(tipoDocumento);
    }

    @Override
    public TipoDocumentoCommand updateTipoDoc(TipoDocumentoCommand tipoDocumentoCommand) {

        TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(tipoDocumentoCommand.getId()).get();
        tipoDocumento.setObserbacion(tipoDocumentoCommand.getObservaciones());
        tipoDocumento.setDescripcion(tipoDocumentoCommand.getDescripcion());
        tipoDocumentoRepository.save(tipoDocumento);
        return new TipoDocumentoCommand(tipoDocumento);
    }

    @Override
    public void deleteTipoDocBYID(Long id) {

        TipoDocumento tipoDocumento = tipoDocumentoRepository.findById(id).get();
        tipoDocumento.setEstadoDelete(true);
        tipoDocumentoRepository.save(tipoDocumento);
    }


}
