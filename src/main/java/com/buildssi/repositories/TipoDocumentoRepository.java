/**
 * @author: raulchoque
 **/
package com.buildssi.repositories;

import com.buildssi.model.TipoDocumento;
import org.springframework.data.repository.CrudRepository;

public interface TipoDocumentoRepository extends CrudRepository<TipoDocumento, Long> {
}
