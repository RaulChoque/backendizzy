package com.buildssi.repositories;

import com.buildssi.model.EmpleadoCargoObra;
import org.springframework.data.repository.CrudRepository;

public interface EmpleadoCargoObraRepository extends CrudRepository<EmpleadoCargoObra, Long> {
}
