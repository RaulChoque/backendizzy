/**
 * @author: raulchoque
 **/
package com.buildssi.repositories;

import com.buildssi.model.Proveedor;
import org.springframework.data.repository.CrudRepository;

public interface ProveedorRepository extends CrudRepository<Proveedor, Long> {
}
