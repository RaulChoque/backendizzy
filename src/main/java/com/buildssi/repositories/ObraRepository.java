package com.buildssi.repositories;

import com.buildssi.model.Obra;
import org.springframework.data.repository.CrudRepository;

public interface ObraRepository extends CrudRepository<Obra, Long> {
}
