package com.buildssi.repositories;

import com.buildssi.model.Avance;
import org.springframework.data.repository.CrudRepository;

public interface AvanceRepository extends CrudRepository<Avance, Long> {
}
