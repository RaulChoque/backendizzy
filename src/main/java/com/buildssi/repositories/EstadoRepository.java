/**
 * @author: raulchoque
 **/
package com.buildssi.repositories;

import com.buildssi.model.Estado;
import org.springframework.data.repository.CrudRepository;

public interface EstadoRepository extends CrudRepository<Estado, Long> {
}
