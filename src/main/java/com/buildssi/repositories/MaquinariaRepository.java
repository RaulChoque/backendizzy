/**
 * @author: raulchoque
 **/
package com.buildssi.repositories;

import com.buildssi.model.Maquinaria;
import org.springframework.data.repository.CrudRepository;

public interface MaquinariaRepository extends CrudRepository<Maquinaria, Long> {
}
