package com.buildssi.repositories;

import com.buildssi.model.Cargo;
import org.springframework.data.repository.CrudRepository;

public interface CargoRepository extends CrudRepository<Cargo, Long> {
}
