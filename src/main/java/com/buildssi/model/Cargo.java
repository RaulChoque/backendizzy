package com.buildssi.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Cargo extends ModelBase{
    private int jerarquia;
    private String descripcion;
    @OneToOne(optional = false)
    private Estado estado;
    @OneToMany(mappedBy = "cargo",fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    private Set<EmpleadoCargoObra> empleadoCargoObras=new HashSet<>();

    public int getJerarquia() {
        return jerarquia;
    }

    public void setJerarquia(int jerarquia) {
        this.jerarquia = jerarquia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set<EmpleadoCargoObra> getEmpleadoCargoObras() {
        return empleadoCargoObras;
    }

    public void setEmpleadoCargoObras(Set<EmpleadoCargoObra> empleadoCargoObras) {
        this.empleadoCargoObras = empleadoCargoObras;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
