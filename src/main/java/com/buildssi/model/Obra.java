package com.buildssi.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Obra extends ModelBase {
    private String nombre;
    private String direccion;
    private String ciudad;
    private double presupuesto;
    @OneToOne(targetEntity = Estado.class)
    private Estado estado;
    @OneToMany(mappedBy = "obra",fetch = FetchType.EAGER,cascade ={CascadeType.ALL})
    private Set<Avance> avances=new HashSet<>();
    //@OneToMany(mappedBy = "obra",fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    //private Set<EmpleadoCargoObra> empleadoCargoObras=new HashSet<>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    /*public Set<EmpleadoCargoObra> getEmpleadoCargoObras() {
        return empleadoCargoObras;
    }

    public void setEmpleadoCargoObras(Set<EmpleadoCargoObra> empleadoCargoObras) {
        this.empleadoCargoObras = empleadoCargoObras;
    }

    public void setAvances(Set<Avance> avances) {
        this.avances = avances;
    }

    public Set<Avance> getAvances() {
        return avances;
    }*/
}
