/**
 * @autor: Raúl Choque
 **/
package com.buildssi.model;

import javax.persistence.*;
import java.util.Date;

@Entity

public class Maquinaria extends ModelBase{

    private String descripcion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActivacion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    private Double valorAdquisicion;

    @Lob
    private Byte[] image;

    @OneToOne(targetEntity = Proveedor.class)
    private Proveedor proveedor;

    @OneToOne(targetEntity = Estado.class)
    private Estado estado;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFechaActivacion(Date fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Double getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(Double valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
}
