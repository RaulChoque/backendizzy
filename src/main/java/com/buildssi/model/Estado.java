/**
 * @autor: Raúl Choque
 **/
package com.buildssi.model;

import javax.persistence.Entity;

@Entity

public class Estado extends ModelBase {

    private String nombre;
    private String descripcion;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
