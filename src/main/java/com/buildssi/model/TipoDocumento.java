/**
 * @autor: Raúl Choque
 **/
package com.buildssi.model;

import javax.persistence.Entity;

@Entity

public class TipoDocumento extends ModelBase {

    private String descripcion;
    private String obserbacion;

    public String getObserbacion() {
        return obserbacion;
    }

    public void setObserbacion(String obserbacion) {
        this.obserbacion = obserbacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
