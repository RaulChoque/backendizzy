package com.buildssi.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Empleado extends ModelBase {
    private String nombre;
    private String apellido;
    private Date fechaDeNacimiento;
    private int numeroDocumento;
    @Lob
    private Byte[] foto;
    @OneToOne(optional = false)
    private Estado estado;
    @OneToMany(mappedBy = "empleado",fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    private Set<EmpleadoCargoObra> empleadoCargoObras=new HashSet<>();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public void setFechaDeNacimiento(Date fechaDeNacimiento) {
        this.fechaDeNacimiento = fechaDeNacimiento;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(int numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Byte[] getFoto() {
        return foto;
    }

    public void setFoto(Byte[] foto) {
        this.foto = foto;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Set<EmpleadoCargoObra> getEmpleadoCargoObras() {
        return empleadoCargoObras;
    }

    public void setEmpleadoCargoObras(Set<EmpleadoCargoObra> empleadoCargoObras) {
        this.empleadoCargoObras = empleadoCargoObras;
    }
}
