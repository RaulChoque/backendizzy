package com.buildssi.bootstrap;

import com.buildssi.model.*;
import com.buildssi.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    ObraRepository obraRepository;
    AvanceRepository avanceRepository;
    TipoDocumentoRepository tipoDocumentoRepository;
    EstadoRepository estadoRepository;
    ProveedorRepository proveedorRepository;
    MaquinariaRepository maquinariaRepository;

    public DevBootstrap(TipoDocumentoRepository tipoDocumentoRepository, EstadoRepository estadoRepository, ProveedorRepository proveedorRepository, ObraRepository obraRepository, AvanceRepository avanceRepository, MaquinariaRepository maquinariaRepository) {
        this.tipoDocumentoRepository = tipoDocumentoRepository;
        this.estadoRepository = estadoRepository;
        this.proveedorRepository = proveedorRepository;
        this.obraRepository=obraRepository;
        this.avanceRepository=avanceRepository;
        this.maquinariaRepository = maquinariaRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        //Estado Obra Activo
        Estado estadoObra1=new Estado();
            estadoObra1.setNombre("Obra");
        estadoObra1.setDescripcion("Activo");
        estadoRepository.save(estadoObra1);

        //Estado Obra Inactivo
        Estado estadoObra2=new Estado();
            estadoObra2.setNombre("Obra");
        estadoObra2.setDescripcion("Inactivo");
        estadoRepository.save(estadoObra2);

        //Obra Numero 1
        Obra obra1=new Obra();
        obra1.setEstado(estadoObra1);
        obra1.setNombre("Obra 1");
        obra1.setDireccion("Direccion 1");
        obra1.setCiudad("Ciudad 1");
        obra1.setPresupuesto(1000000.00);
        obraRepository.save(obra1);

        //Estado Avance Activo
        Estado estadoAvance1=new Estado();
            estadoAvance1.setNombre("Avance");
        estadoAvance1.setDescripcion("Activo");
        estadoRepository.save(estadoAvance1);

        //Estado Avance En Ejecucion
        Estado estadoAvance2=new Estado();
            estadoAvance2.setNombre("Avance");
        estadoAvance2.setDescripcion("En ejecucion");
        estadoRepository.save(estadoAvance2);

        //Estado Avance Concluido
        Estado estadoAvance3=new Estado();
            estadoAvance3.setNombre("Avance");
        estadoAvance3.setDescripcion("Concluido");
        estadoRepository.save(estadoAvance3);

        //Obra Numero 1, Avance 1
        Avance avance11=new Avance();
        avance11.setEstado(estadoAvance3);
        avance11.setObra(obra1);
        avance11.setFecha(new Date(2010, 1, 1));
        avance11.setDescripcion("Inicio de Obra 1");
        avance11.setCosto(100000.00);
        avanceRepository.save(avance11);

        //Obra Numero 1, Avance 2
        Avance avance12=new Avance();
        avance12.setEstado(estadoAvance2);
        avance12.setObra(obra1);
        avance12.setFecha(new Date(2010, 2, 1));
        avance12.setDescripcion("Primer Avance de la Obra 1");
        avance12.setCosto(100000.00);
        avanceRepository.save(avance12);

        //typeDocuments
        TipoDocumento ci = new TipoDocumento();
        ci.setDescripcion("CI");
        ci.setObserbacion("Cedula de Identidad");

        TipoDocumento nit = new TipoDocumento();
        nit.setDescripcion("NIT");
        nit.setObserbacion("Numero de Identificacion Tributaria");

        TipoDocumento pasaporte = new TipoDocumento();
        pasaporte.setDescripcion("Pasaporte");
        pasaporte.setObserbacion("documento confeccionado por un gobierno que permite a sus ciudadanos salir de un país e ingresar en otro.");

        TipoDocumento dui = new TipoDocumento();
        dui.setDescripcion("DUI");
        dui.setObserbacion("Declaracion Unica de Importaciones");

        tipoDocumentoRepository.save(ci);
        tipoDocumentoRepository.save(nit);
        tipoDocumentoRepository.save(pasaporte);
        tipoDocumentoRepository.save(dui);

        //provider
        Proveedor semaico = new Proveedor();
        semaico.setNombreComercial("Semaico Ltda.");
        semaico.setRazonSocial("Luis Arce");
        semaico.setNumeroDocumento("12345asd");
        semaico.setDireccion("Av. Republica Nº 435");
        semaico.setDiasPlazo(3);
        semaico.setTelefono(4756710);
        semaico.setCorreo("semaico@gmail.com");
        semaico.setTipoDocumento(ci);

        Proveedor hormimaq = new Proveedor();
        hormimaq.setNombreComercial("Hormimaq");
        hormimaq.setRazonSocial("Aranivar Fuentes");
        hormimaq.setNumeroDocumento("12345asd");
        hormimaq.setDireccion("Av. Blanco Galindo Km. 2");
        hormimaq.setDiasPlazo(5);
        hormimaq.setTelefono(4202020);
        hormimaq.setCorreo("hormimaq@gmail.com");
        hormimaq.setTipoDocumento(nit);

        Proveedor constantin = new Proveedor();
        constantin.setNombreComercial("Constantin");
        constantin.setRazonSocial("Marcia Valencia");
        constantin.setNumeroDocumento("12345asd");
        constantin.setDireccion("Av. Petrolera KM. 1");
        constantin.setDiasPlazo(1);
        constantin.setTelefono(4777777);
        constantin.setCorreo("constantin@gmail.com");
        constantin.setTipoDocumento(dui);

        proveedorRepository.save(semaico);
        proveedorRepository.save(hormimaq);
        proveedorRepository.save(constantin);


        //state from Machinery
        Estado enObra = new Estado();
        enObra.setNombre("Maquinaria");
        enObra.setDescripcion("En Obra");

        Estado enAlmacen = new Estado();
        enAlmacen.setNombre("Maquinaria");
        enAlmacen.setDescripcion("En Almacen");

        Estado mantenimiento = new Estado();
        mantenimiento.setNombre("Maquinaria");
        mantenimiento.setDescripcion("Mantenimiento");

        estadoRepository.save(enObra);
        estadoRepository.save(enAlmacen);
        estadoRepository.save(mantenimiento);

        //machinery
        Maquinaria mescladora = new Maquinaria();
        mescladora.setDescripcion("Mescladora Electrica");
        mescladora.setFechaActivacion(new Date(2012, 3, 12));
        mescladora.setFechaBaja(new Date(2014, 3, 12));
        mescladora.setValorAdquisicion(1200.4);
        mescladora.setProveedor(semaico);
        mescladora.setEstado(enAlmacen);

        Maquinaria palaCargadora = new Maquinaria();
        palaCargadora.setDescripcion("Pala Cargadora");
        palaCargadora.setFechaActivacion(new Date(2012, 3, 12));
        palaCargadora.setFechaBaja(new Date(2017, 3, 12));
        palaCargadora.setValorAdquisicion(1200.4);
        palaCargadora.setProveedor(semaico);
        palaCargadora.setEstado(enAlmacen);

        Maquinaria andamios = new Maquinaria();
        andamios.setDescripcion("Andamios metalicos");
        andamios.setFechaActivacion(new Date(2012, 3, 12));
        andamios.setFechaBaja(new Date(2013, 3, 12));
        andamios.setValorAdquisicion(1200.4);
        andamios.setProveedor(semaico);
        andamios.setEstado(enAlmacen);

        Maquinaria amoladora = new Maquinaria();
        amoladora.setDescripcion("Amoladora electrica");
        amoladora.setFechaActivacion(new Date(2012, 3, 12));
        amoladora.setFechaBaja(new Date(2013, 3, 12));
        amoladora.setValorAdquisicion(1200.4);
        amoladora.setProveedor(semaico);
        amoladora.setEstado(enAlmacen);

        Maquinaria soldadora = new Maquinaria();
        soldadora.setDescripcion("Soldadora");
        soldadora.setFechaActivacion(new Date(2012, 3, 12));
        soldadora.setFechaBaja(new Date(2015, 3, 12));
        soldadora.setValorAdquisicion(1200.4);
        soldadora.setProveedor(semaico);
        soldadora.setEstado(enObra);

        maquinariaRepository.save(soldadora);
        maquinariaRepository.save(amoladora);
        maquinariaRepository.save(palaCargadora);
        maquinariaRepository.save(mescladora);
        maquinariaRepository.save(andamios);


    }
}
