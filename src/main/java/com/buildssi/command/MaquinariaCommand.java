/**
 * @autor: Raúl Choque
 **/
package com.buildssi.command;

import com.buildssi.model.Maquinaria;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class MaquinariaCommand {

    private Long id;
    private String descripcion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActivacion;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaBaja;
    private Double valorAdquisicion;
    private Long idProveedor;
    private String nombreProveedor;
    private Long idEstado;
    private String nombreEstado;

    public MaquinariaCommand() {
    }

    public MaquinariaCommand(Maquinaria maquinaria) {

        this.id = maquinaria.getId();
        this.descripcion = maquinaria.getDescripcion();

        this.fechaActivacion = maquinaria.getFechaActivacion();
        this.fechaBaja = maquinaria.getFechaBaja();
        this.valorAdquisicion = maquinaria.getValorAdquisicion();
        this.idProveedor = maquinaria.getProveedor().getId();
        this.nombreProveedor = maquinaria.getProveedor().getNombreComercial();
        this.idEstado = maquinaria.getEstado().getId();
        this.nombreEstado = maquinaria.getEstado().getDescripcion();

    }

    public Maquinaria toModel() {

        Maquinaria maquinaria = new Maquinaria();
        maquinaria.setDescripcion(getDescripcion());
        maquinaria.setFechaActivacion(getFechaActivacion());
        maquinaria.setFechaBaja(getFechaBaja());
        maquinaria.setValorAdquisicion(getValorAdquisicion());

        return maquinaria;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFechaActivacion() {
        return fechaActivacion;
    }

    public void setFechaActivacion(Date fechaActivacion) {
        this.fechaActivacion = fechaActivacion;
    }

    public Date getFechaBaja() {
        return fechaBaja;
    }

    public void setFechaBaja(Date fechaBaja) {
        this.fechaBaja = fechaBaja;
    }

    public Double getValorAdquisicion() {
        return valorAdquisicion;
    }

    public void setValorAdquisicion(Double valorAdquisicion) {
        this.valorAdquisicion = valorAdquisicion;
    }

    public Long getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public Long getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Long idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
}
