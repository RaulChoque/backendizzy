package com.buildssi.command;

import com.buildssi.model.*;

import java.util.Date;

public class ObraCommand extends ModelBase {
    private String nombre;
    private String direccion;
    private String ciudad;
    private double presupuesto;

    private Long idEstadoObra;
    private Estado estadoObra;

    private Date fechaAvance;
    private String descripcionAvance;
    private double costoAvance;

    private Date fechaAsignaciones;

    public ObraCommand(){}

    public ObraCommand(Obra obra){
        setId(obra.getId());
        setVersion(obra.getVersion());
        setCreatedOn(obra.getCreatedOn());
        setUpdatedOn(obra.getUpdatedOn());

        this.setNombre(obra.getNombre());
        this.setDireccion(obra.getDireccion());
        this.setCiudad(obra.getCiudad());
        this.setPresupuesto(obra.getPresupuesto());

        this.idEstadoObra=obra.getEstado().getId();
    }

    public Obra toModel(){
        Obra obra=new Obra();

        obra.setId(getId());
        obra.setVersion(getVersion());
        obra.setCreatedOn(getCreatedOn());
        obra.setUpdatedOn(getUpdatedOn());

        obra.setNombre(getNombre());
        obra.setDireccion(getDireccion());
        obra.setCiudad(getCiudad());
        obra.setPresupuesto(getPresupuesto());

        obra.setEstado(getEstadoObra());

        return obra;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Estado getEstadoObra() {
        return estadoObra;
    }

    public void setEstadoObra(Estado estadoObra) {
        this.estadoObra = estadoObra;
    }

    public Long getIdEstadoObra() {
        return idEstadoObra;
    }

    public void setIdEstadoObra(Long idEstadoObra) {
        this.idEstadoObra = idEstadoObra;
    }
}
