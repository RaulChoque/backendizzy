package com.buildssi.command;

import com.buildssi.model.*;

import java.util.Date;

public class EmpleadoCargoObraCommand extends ModelBase {
    private Date fechaAsignacion;

    private String nombreObra;

    private String descripcionCargo;

    private String nombreEmpleado;

    public EmpleadoCargoObraCommand(EmpleadoCargoObra empleadoCargoObra){
        setId(empleadoCargoObra.getId());
        setVersion(empleadoCargoObra.getVersion());
        setCreatedOn(empleadoCargoObra.getCreatedOn());
        setUpdatedOn(empleadoCargoObra.getUpdatedOn());

        fechaAsignacion=empleadoCargoObra.getFechaAsignacion();

        Obra obra=new Obra();
        nombreObra=obra.getNombre();

        Cargo cargo=new Cargo();
        descripcionCargo=cargo.getDescripcion();

        Empleado empleado=new Empleado();
        nombreEmpleado=empleado.getNombre();
    }

    public EmpleadoCargoObra toEmpleadoCargoObra(){
        EmpleadoCargoObra empleadoCargoObra=new EmpleadoCargoObra();

        empleadoCargoObra.setId(getId());
        empleadoCargoObra.setVersion(getVersion());
        empleadoCargoObra.setCreatedOn(getCreatedOn());
        empleadoCargoObra.setUpdatedOn(getUpdatedOn());

        empleadoCargoObra.setFechaAsignacion(getFechaAsignacion());

        return empleadoCargoObra;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }
}
