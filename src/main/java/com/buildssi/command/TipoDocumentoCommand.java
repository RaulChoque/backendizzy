/**
 * @autor: Raúl Choque
 **/
package com.buildssi.command;

import com.buildssi.model.TipoDocumento;

public class TipoDocumentoCommand {
    private Long id;
    private String descripcion;
    private String observaciones;

    public TipoDocumentoCommand(TipoDocumento tipoDocumento) {

        this.id = tipoDocumento.getId();
        this.descripcion = tipoDocumento.getDescripcion();
        this.observaciones = tipoDocumento.getObserbacion();
    }

    public TipoDocumentoCommand() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public TipoDocumento toModel() {

        TipoDocumento tipoDocumento = new TipoDocumento();
        tipoDocumento.setDescripcion(this.getDescripcion());
        tipoDocumento.setObserbacion(this.getObservaciones());
        return tipoDocumento;
    }
}
