package com.buildssi.command;

import com.buildssi.model.Avance;
import com.buildssi.model.Estado;
import com.buildssi.model.ModelBase;
import com.buildssi.model.Obra;

import java.util.Date;

public class AvanceCommand {
    private Date fecha;
    private String descripcion;
    private double costo;
    private Long Id;
    private String estadoAvance;
    private String obraNombre;
    private Obra obra;
    private Long idObra;


    public AvanceCommand(Avance avance){
        this.Id=avance.getId();
        //setVersion(avance.getVersion());
        //setCreatedOn(avance.getCreatedOn());
        //setUpdatedOn(avance.getUpdatedOn());

        this.fecha=avance.getFecha();
        this.descripcion=avance.getDescripcion();
        this.costo=avance.getCosto();

        this.idObra = avance.getObra().getId();

        //Estado estado=new Estado();
        //estadoAvance=estado.getDescripcion();

        //Obra obra=new Obra();
        //obraNombre=obra.getNombre();
    }

    public AvanceCommand() {
    }

    public Avance toAvance(Avance avance){
        //Avance avance=new Avance();

        //avance.setId(getId());
        //avance.setVersion(getVersion());
        //avance.setCreatedOn(getCreatedOn());
        //avance.setUpdatedOn(getUpdatedOn());

        avance.setFecha(getFecha());
        avance.setDescripcion(getDescripcion());
        avance.setCosto(getCosto());
        avance.setObra(getObra());

        return avance;
    }

    public Avance toModel(){
        Avance avance=new Avance();

        //avance.setId(getId());
        //avance.setVersion(getVersion());
        //avance.setCreatedOn(getCreatedOn());
        //avance.setUpdatedOn(getUpdatedOn());

        avance.setFecha(getFecha());
        avance.setDescripcion(getDescripcion());
        avance.setCosto(getCosto());
        avance.setObra(getObra());

        return avance;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getCosto() {
        return costo;
    }

    public Obra getObra() {
        return obra;
    }

    public void setObra(Obra obra) {
        this.obra = obra;
    }

    public Long getIdObra() {
        return idObra;
    }

    public void setIdObra(Long idObra) {
        this.idObra = idObra;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }
}
