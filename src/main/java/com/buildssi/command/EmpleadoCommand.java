package com.buildssi.command;

import com.buildssi.model.Empleado;
import com.buildssi.model.EmpleadoCargoObra;
import com.buildssi.model.Estado;
import com.buildssi.model.ModelBase;
import org.apache.tomcat.util.codec.binary.Base64;

import java.util.Date;

public class EmpleadoCommand extends ModelBase {
    private String nombre;
    private String apellido;
    private Date fechaDeNacimiento;
    private int numeroDocumento;
    private String foto;

    private String estadoEmpleado;

    private Date fechaAsignaciones;

    public EmpleadoCommand(Empleado empleado){
        setId(empleado.getId());
        setVersion(empleado.getVersion());
        setCreatedOn(empleado.getCreatedOn());
        setUpdatedOn(empleado.getUpdatedOn());

        nombre=empleado.getNombre();
        apellido=empleado.getApellido();
        fechaDeNacimiento=empleado.getFechaDeNacimiento();
        numeroDocumento=empleado.getNumeroDocumento();

        Estado estado=new Estado();
        estadoEmpleado=estado.getDescripcion();

        for(EmpleadoCargoObra empleadoCargoObra:empleado.getEmpleadoCargoObras()){
            fechaAsignaciones=empleadoCargoObra.getFechaAsignacion();
        }
        setImageBase64(empleado);
    }

    private void setImageBase64(Empleado empleado) {
        if (empleado.getFoto() != null) {
            byte[] bytes = new byte[empleado.getFoto().length];
            for (int i = 0; i < empleado.getFoto().length; i++) {
                bytes[i] = empleado.getFoto()[i];
            }
            String imageStr = Base64.encodeBase64String(bytes);
            this.setFoto(imageStr);
        }
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Empleado toEmpleado(){
        Empleado empleado=new Empleado();

        empleado.setId(getId());
        empleado.setVersion(getVersion());
        empleado.setCreatedOn(getCreatedOn());
        empleado.setUpdatedOn(getUpdatedOn());

        empleado.setNombre(getNombre());
        empleado.setApellido(getApellido());
        empleado.setFechaDeNacimiento(getFechaDeNacimiento());
        empleado.setNumeroDocumento(getNumeroDocumento());

        return empleado;
    }

    public Date getFechaDeNacimiento() {
        return fechaDeNacimiento;
    }

    public int getNumeroDocumento() {
        return numeroDocumento;
    }
}


