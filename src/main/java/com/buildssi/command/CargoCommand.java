package com.buildssi.command;

import com.buildssi.model.Cargo;
import com.buildssi.model.EmpleadoCargoObra;
import com.buildssi.model.Estado;
import com.buildssi.model.ModelBase;

import java.util.Date;

public class CargoCommand extends ModelBase {
    private int jerarquia;
    private String descripcion;

    private String estadoCargo;

    private Date fechaAsignaciones;

    public CargoCommand(Cargo cargo){
        setId(cargo.getId());
        setVersion(cargo.getVersion());
        setCreatedOn(cargo.getCreatedOn());
        setUpdatedOn(cargo.getUpdatedOn());

        jerarquia=cargo.getJerarquia();
        descripcion=cargo.getDescripcion();

        Estado estado=new Estado();
        estadoCargo=estado.getDescripcion();

        for(EmpleadoCargoObra empleadoCargoObra:cargo.getEmpleadoCargoObras()){
            fechaAsignaciones=empleadoCargoObra.getFechaAsignacion();
        }
    }

    public Cargo toCargo(){
        Cargo cargo=new Cargo();

        cargo.setId(getId());
        cargo.setVersion(getVersion());
        cargo.setCreatedOn(getCreatedOn());
        cargo.setUpdatedOn(getUpdatedOn());

        cargo.setJerarquia(getJerarquia());
        cargo.setDescripcion(getDescripcion());

        return cargo;
    }

    public int getJerarquia() {
        return jerarquia;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
