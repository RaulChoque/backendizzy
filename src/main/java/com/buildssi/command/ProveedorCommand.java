/**
 * @autor: Raúl Choque
 **/
package com.buildssi.command;

import com.buildssi.model.Proveedor;
import com.buildssi.model.TipoDocumento;

public class ProveedorCommand {

    private Long id;
    private String nombreComercial;
    private String razonSocial;
    private String numeroDocumento;
    private String direccion;
    private Integer diasPlazo;
    private Integer telefono;
    private String correo;
    private TipoDocumento tipoDocumento;
    private Long idTipoDocumento;
    private String descripcionTipoDocumento;

    public ProveedorCommand() {
    }

    public ProveedorCommand(Proveedor proveedor) {

        this.id = proveedor.getId();
        this.nombreComercial = proveedor.getNombreComercial();
        this.razonSocial = proveedor.getRazonSocial();
        this.numeroDocumento = proveedor.getNumeroDocumento();
        this.direccion = proveedor.getDireccion();
        this.diasPlazo = proveedor.getDiasPlazo();
        this.telefono = proveedor.getTelefono();
        this.correo = proveedor.getCorreo();
        this.idTipoDocumento = proveedor.getTipoDocumento().getId();
        this.descripcionTipoDocumento = proveedor.getTipoDocumento().getDescripcion();
    }

    public Proveedor toModel() {

        Proveedor proveedor = new Proveedor();
        proveedor.setNombreComercial(getNombreComercial());
        proveedor.setRazonSocial(getRazonSocial());
        proveedor.setNumeroDocumento(getNumeroDocumento());
        proveedor.setDireccion(getDireccion());
        proveedor.setDiasPlazo(getDiasPlazo());
        proveedor.setTelefono(getTelefono());
        proveedor.setCorreo(getCorreo());
        proveedor.setTipoDocumento(getTipoDocumento());
        return proveedor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getDiasPlazo() {
        return diasPlazo;
    }

    public void setDiasPlazo(Integer diasPlazo) {
        this.diasPlazo = diasPlazo;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Long getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(Long idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getDescripcionTipoDocumento() {
        return descripcionTipoDocumento;
    }

    public void setDescripcionTipoDocumento(String descripcionTipoDocumento) {
        this.descripcionTipoDocumento = descripcionTipoDocumento;
    }
}
