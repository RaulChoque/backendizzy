/**
 * @autor: Raúl Choque
 **/
package com.buildssi.command;

import com.buildssi.model.Estado;

public class EstadoCommand {
    private Long id;
    private String nombre;
    private String descripcion;

    public EstadoCommand(Estado estado) {

        this.id = estado.getId();
        this.nombre = estado.getNombre();
        this.descripcion = estado.getDescripcion();
    }

    public EstadoCommand() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Estado toModel(){
        Estado newEstado = new Estado();
        newEstado.setNombre(this.getNombre());
        newEstado.setDescripcion(this.getDescripcion());
        return newEstado;
    }
}
