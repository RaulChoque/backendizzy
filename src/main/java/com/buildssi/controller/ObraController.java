package com.buildssi.controller;

import com.buildssi.command.ObraCommand;
import com.buildssi.model.Obra;
import com.buildssi.services.ObraService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/obras")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class ObraController {

    private ObraService obraService;

    public ObraController(ObraService obraService) {
        this.obraService = obraService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObras() {
        List<ObraCommand> obraCommands=obraService.getObras();
        return Response.ok(obraCommands).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObraById(@PathParam("id") @NotNull Long id) {
        ObraCommand obraCommand = obraService.getObraByID(id);
        return Response.ok(obraCommand).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createObra(ObraCommand obraCommand) {
        ObraCommand newObraCommand = obraService.createObra(obraCommand);
        return Response.ok(newObraCommand).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateObra(ObraCommand obraCommand) {
        ObraCommand newObraCommand = obraService.updateObra(obraCommand);
        return Response.ok(newObraCommand).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteObra(@PathParam("id") @NotNull long id) {
        obraService.deleteObra(id);
        return Response.noContent().build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }
    /*private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }*/
}
