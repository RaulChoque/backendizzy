/**
 * @autor: Raúl Choque
 **/
package com.buildssi.controller;

import com.buildssi.command.EstadoCommand;
import com.buildssi.services.EstadoService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Controller
@Path("/estados")

public class EstadoController {

    EstadoService estadoService;

    public EstadoController(EstadoService estadoService) {
        this.estadoService = estadoService;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEstados(){

        List<EstadoCommand> listEstados = estadoService.getEstados();
        return Response.ok(listEstados).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEstadoById(@PathParam("id") @NotNull Long id) {

        EstadoCommand newEstadoCommand = estadoService.getEstadoByID(id);
        return Response.ok(newEstadoCommand).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createsEstado(EstadoCommand estadoCommand){

        EstadoCommand newEstadoCommand = estadoService.createEstado(estadoCommand);
        return Response.ok(newEstadoCommand).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateEstados(EstadoCommand estadoCommand) {

        EstadoCommand newEstadoCommand = estadoService.updateEstado(estadoCommand);
        return Response.ok(newEstadoCommand).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEstado(@PathParam("id") @NotNull Long id){

        estadoService.deleteEstById(id);
        return Response.noContent().build();
    }

}
