package com.buildssi.controller;

import com.buildssi.command.AvanceCommand;
import com.buildssi.model.Avance;
import com.buildssi.services.AvanceService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/avances")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@CrossOrigin
public class AvanceController {

    AvanceService avanceService;

    public AvanceController(AvanceService avanceService) {
        this.avanceService = avanceService;
    }

    @GET
    public Response getAvances() {
        List<AvanceCommand> listAvances = avanceService.getAvances();
        return Response.ok(listAvances).build();
    }
    @GET
    @Path("/obra/{id}")
    public Response getAvancesToObra(@PathParam("id") @NotNull Long idObra) {
        List<AvanceCommand> listAvances = avanceService.getAvancesToObra(idObra);
        return Response.ok(listAvances).build();
    }


    @GET
    @Path("/{id}")
    public Response getAvanceById(@PathParam("id") @NotNull Long id) {
        AvanceCommand avanceCommand = avanceService.getAvanceByID(id);
        return Response.ok(avanceCommand).build();
    }

    @POST
    public Response addAvance(AvanceCommand avanceCommand) {
       // Avance avance = avanceService.save(avanceCommand.toAvance());
        //return new AvanceCommand(avance);
        AvanceCommand newAvanceCommand = avanceService.createAvance(avanceCommand);
        return Response.ok(newAvanceCommand).build();
    }

    @PUT
    public Response updateAvance(AvanceCommand avanceCommand) {
        AvanceCommand newAvanceCommand = avanceService.updateAvance(avanceCommand);
        return Response.ok(newAvanceCommand).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAvance(@PathParam("id") @NotNull Long id) {
        avanceService.deleteAvance(id);
        return Response.noContent().build();
    }

    /*private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }*/

}
