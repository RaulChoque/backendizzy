/**
 * @autor: Raúl Choque
 **/
package com.buildssi.controller;

import com.buildssi.command.TipoDocumentoCommand;
import com.buildssi.services.TipoDocumentoService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Controller
@Path("/tipodocumentos")

public class TipoDocumentoController {

    TipoDocumentoService tipoDocumentoService;

    public TipoDocumentoController(TipoDocumentoService tipoDocumentoService) {

        this.tipoDocumentoService = tipoDocumentoService;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTipoDocumento() {

        List<TipoDocumentoCommand> listTipoDoc = tipoDocumentoService.getTipoDoc();
        return Response.ok(listTipoDoc).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTipoDocById(@PathParam("id") @NotNull Long id) {

        TipoDocumentoCommand tipoDocumentoCommand = tipoDocumentoService.getTipoDocById(id);
        return Response.ok(tipoDocumentoCommand).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createTipoDoc(TipoDocumentoCommand tipoDocumentoCommand) {

        TipoDocumentoCommand newTipoDocCommand = tipoDocumentoService.createTipoDoc(tipoDocumentoCommand);
        return Response.ok(newTipoDocCommand).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateTipoDoc(TipoDocumentoCommand tipoDocumentoCommand) {

        TipoDocumentoCommand newTipoDocCom = tipoDocumentoService.updateTipoDoc(tipoDocumentoCommand);
        return Response.ok(newTipoDocCom).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteTipoDocumento(@PathParam("id") @NotNull Long id) {

        tipoDocumentoService.deleteTipoDocBYID(id);
        return Response.noContent().build();

    }

}
