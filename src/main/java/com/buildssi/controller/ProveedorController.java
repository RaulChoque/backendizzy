/**
 * @autor: Raúl Choque
 **/
package com.buildssi.controller;

import com.buildssi.command.ProveedorCommand;
import com.buildssi.services.ProveedorService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Controller
@Path("/proveedores")

public class ProveedorController {

    ProveedorService proveedorService;

    public ProveedorController(ProveedorService proveedorService) {
        this.proveedorService = proveedorService;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProveedores() {

        List<ProveedorCommand> listProveedores = proveedorService.getProveedores();
        return Response.ok(listProveedores).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProveedorById(@PathParam("id") @NotNull Long id) {
        ProveedorCommand proveedorCommand = proveedorService.getProveedorByID(id);
        return Response.ok(proveedorCommand).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createProveedor(ProveedorCommand proveedorCommand) {
        ProveedorCommand newProveedorCommand = proveedorService.createProveedor(proveedorCommand);
        return Response.ok(newProveedorCommand).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateProveedor(ProveedorCommand proveedorCommand) {

        ProveedorCommand newProveedorCommand = proveedorService.updateProveedor(proveedorCommand);
        return Response.ok(newProveedorCommand).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteProveedor(@PathParam("id") @NotNull Long id) {

        proveedorService.deleteProveedor(id);
        return Response.noContent().build();
    }
}
