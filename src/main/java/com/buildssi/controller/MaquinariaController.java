/**
 * @autor: Raúl Choque
 **/
package com.buildssi.controller;

import com.buildssi.command.MaquinariaCommand;
import com.buildssi.services.MaquinariaService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Controller
@Path("/maquinarias")

public class MaquinariaController {

    MaquinariaService maquinariaService;

    public MaquinariaController(MaquinariaService maquinariaService) {
        this.maquinariaService = maquinariaService;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMaquinarias() {
        List<MaquinariaCommand> listMaquinaria = maquinariaService.getMaquinarias();
        return Response.ok(listMaquinaria).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMaquinariaById(@PathParam("id") @NotNull Long id) {

        MaquinariaCommand maquinariaCommand = maquinariaService.getMaquinariaByID(id);
        return Response.ok(maquinariaCommand).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createMaquinaria(MaquinariaCommand maquinariaCommand) {

        MaquinariaCommand newMaquinariaCommand = maquinariaService.createMaquinaria(maquinariaCommand);
        return Response.ok(newMaquinariaCommand).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateMaquinaria(MaquinariaCommand maquinariaCommand) {

        MaquinariaCommand newMaquinariaCommand = maquinariaService.updateMaquinaria(maquinariaCommand);
        return Response.ok(newMaquinariaCommand).build();
    }

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteMaquinariaById(@PathParam("id") @NotNull Long id) {

        maquinariaService.deleteMaquinariaById(id);
        return Response.noContent().build();
    }

}
